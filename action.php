<?php
// define database connection
$con=mysqli_connect("localhost","root","root","db_haystack");
// Check connection
if (mysqli_connect_errno()) {
  
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  
} else {
	if( empty( trim($_POST['username'] )) && empty(trim( $_POST['email'] )) && empty( $_POST['telephone'] ) && empty( $_POST['message'] )) {
	  print "All fields are required !!";
	  exit();
	} else {
	  
	  $username=trim($_POST['username']);
	  $email=trim($_POST['email']);
	  $telephone = $_POST['telephone'];
	  $message = stripslashes($_POST['message']);
	  $date = date("Y-m-d H:i:s");
	  // phone number validation
	  if( $telephone ) {
	    if( is_numeric($telephone) && strlen($telephone) == 10 && strlen($telephone) > 0 ) {} else {
	      print "Phone number is required, Enter 10 digit numeric value !!!";
	      exit();
	    }
	  }
	  //email validation
	  if( $email ) {
	    if (filter_var($email, FILTER_VALIDATE_EMAIL)==false) {
	      print "Email field is required, enter valid email address e.g user123@gmail.com !!!";
	      exit();
	    }
	  }
	  
	  //text validation on message field
	  if( $message ) {
	    if ( empty($message) ) {
	      print "Enter your message !!!!";
	      exit();
	    }
	  }
	  
	  // escape variables for security 
	  $sql="INSERT INTO contacts(name,email,telephone_no,message,added_on,updated_on) VALUES ('".$username."','".$email."','".$telephone."','".$message."','".$date."','".$date."')";
	  if (!mysqli_query($con,$sql)) {
	    die('Error: ' . mysqli_error($con));
	  }	  
	  $message = '<html><body>';
	  $message .= '<img style="background: #153254;" src="http://mastersoftwaretechnologies.com/heystack/images/logo.png" alt="Website Change Request" />';
	  $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
	  $message .= "<tr style='background: #eee;'><td><strong>Name:</strong> </td><td>" . strip_tags($_POST['username']) . "</td></tr>";
	  $message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($_POST['email']) . "</td></tr>";
	  $message .= "<tr><td><strong>Phone Number:</strong> </td><td>" . strip_tags($_POST['telephone']) . "</td></tr>";
	  $message .= "<tr><td><strong>Message:</strong> </td><td>" . strip_tags($_POST['message']) . "</td></tr>";
	  $message .= "</table>";
	  $message .= "</body></html>";
	  
	  $to = 'mss.manishasharmaS@gmail.com';			
	  $subject = 'Heystack, New Contact request';			
	  $headers = "From: " . $_POST['email'] . "\r\n";
	  $headers .= "Reply-To: ". strip_tags($_POST['email']) . "\r\n";
	  $headers .= "MIME-Version: 1.0\r\n";
	  $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";	  
	  
	  if (mail($to, $subject, $message, $headers)) {
              echo 'Your message has been sent, we will get back to you soon.';
	      ?>
		<script>
		  document.getElementById("contactForm").reset();
		</script>
	      <?php
          } else {
              echo 'There was a problem sending the email.';
          }
	  mysqli_close($con);
	}
}
?>
